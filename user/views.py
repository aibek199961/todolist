from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from user.serializers import UserSerializer
from user.models import User
from user.permissions import IsUserOwnerOrReadOnly

class UserView(ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsUserOwnerOrReadOnly, )
