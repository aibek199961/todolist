from rest_framework import serializers
from user.models import User
from category.models import Category

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username')

class CategorySerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True, many=False)

    class Meta:
        model = Category
        fields = ('id', 'name', 'description', 'owner')

    def create(self, validated_data):
        user = self.context.get('request').user
        category = Category.objects.create(owner=user, **validated_data)
        return category
        