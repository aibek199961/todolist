from django.db import models
from django.conf import settings


class Category(models.Model):
    name = models.CharField('Название', max_length=255)
    description = models.TextField('Описание')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'category_owner')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name
