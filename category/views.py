from django.shortcuts import render
from category.serializers import CategorySerializer
from rest_framework.viewsets import ModelViewSet
from category.models import Category
from django.db.models import F 
from category.permissions import IsCategoryOwnerOrReadOnly
from rest_framework.response import Response


class CategoryView(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsCategoryOwnerOrReadOnly, )

    def list(self, request):
        user = request.user
        category = Category.objects.filter(user=user) 
        serializer = CategorySerializer(category, many=True) 
        return Response(serializer.data)


