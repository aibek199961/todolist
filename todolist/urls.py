from django.contrib import admin
from django.urls import path, include
from rest_auth.views import LoginView
from rest_auth.registration.views import RegisterView
from category.views import CategoryView
from user.views import UserView
from task.views import TaskView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('signin/', LoginView.as_view(), name='rest_login'),
    path('signup/', RegisterView.as_view(), name='rest_register'),
    path('user/<int:pk>', UserView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path('category/', CategoryView.as_view({'get': 'list', 'post': 'create'})),
    path('category/<int:pk>', CategoryView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path('task/', TaskView.as_view({'get': 'list', 'post': 'create'})), 
    path('task/<int:pk>', TaskView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
]
