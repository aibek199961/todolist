from django.shortcuts import render
from task.serializers import TaskSerializer
from rest_framework.viewsets import ModelViewSet
from task.models import Task
from django.db.models import F 
from task.permissions import IsTaskOwnerOrReadOnly
from rest_framework.response import Response


class TaskView(ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsTaskOwnerOrReadOnly, )

    # def list(self, request):
    #     user = request.user
    #     task = Task.objects.filter(owner=user) 
    #     serializer = TaskSerializer(task, many=True) 
        # return Response(serializer.data)

