from django.db import models
from django.conf import settings


class Task(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'task_owner')
    headline = models.CharField('Заголовок', max_length=255)
    description = models.TextField('Описание')
    deadline = models.DateTimeField('Дедлайн', null=True)
    category = models.ForeignKey('category.Category', models.CASCADE, 'task_category', null=True)
    
    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'

    def __str__(self):
        return self.headline



