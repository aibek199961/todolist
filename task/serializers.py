from rest_framework import serializers
from user.models import User
from task.models import Task
from category.models import Category

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username')


class CategorySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Category
        fields = ('id', 'name')


class TaskSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True, many=False)
    task_category = CategorySerializer(read_only=True, many=True)

    class Meta:
        model = Task
        fields = ('id', 'owner', 'headline', 'description', 'deadline', 'task_category')

    def create(self, validated_data):
        user = self.context.get('request').user
        task = Task.objects.create(owner=user, **validated_data)
        return task

    
        